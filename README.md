# Transient BLE Sensor Node

This Bluetooth Low Energy (BLE) sensor node monitors ambient conditions like temperature, humidity, pressure and illuminance.
The measured sensor readings, as well as selected historical reading buffered in the FRAM memory are broadcast periodically as BLE beacons.
The sensor node operates fully autonomous by harvesting from ambient light.


## Repository Organization

- [`app/`](app/) - Android Studio project of the application to collect, decode and log the sensor node data broadcast as BLE advertisements.
- [`firmware/`](firmware/) - Sensor node firmware sources to compile with the contiki-ng embedded operating system.
- [`hardware/`](hardware/) - Altium Designer project files of the sensor node hardware.


## Harvesting-Aware Infrastructure-Less Communication

The presented platform was successfully used in a real-world deployment for testing and evaluation of a novel harvesting-aware infrastructure-less communication scheme for batteryless sensors. Detailed presentation of the communication scheme and extensive evaluation results are found in the following journal article:

> L. Sigrist, R. Ahmed, A. Gomez, and L. Thiele.
> *Harvesting-Aware Optimal Communication Scheme for Infrastructure-Less Sensing*.
> In ACM Transactions on Internet of Things (TIOT), 2020.
> DOI: [10.1145/3395928](https://doi.org/10.1145/3395928)


## Project Contributors

* Alfonso Blanco <alfonso.blanco@ee.ethz.ch>
* Oliver Brunecker <brolive@student.ethz.ch>
* Andres Gomez <gomeza@ethz.ch>
* Lukas Sigrist <lukas.sigrist@tik.ee.ethz.ch>

Contact: Lukas Sigrist


## Changelog

* 2020-04-28: first public release of the final transient BLE node project


## Copyright and License

* Copyright: (c) 2017-2020, ETH Zurich, Computer Engineering Group and Microelectronics Design Center.
* License: Creative Commons Attribution 4.0 International License (<http://creativecommons.org/licenses/by/4.0/>)
