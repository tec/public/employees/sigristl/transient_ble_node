# Transient BLE Sensor Node


## Detailed Project Description

This Bluetooth Low Energy (BLE) sensor node monitors ambient conditions like temperature, humidity, pressure and illuminance.
The measured sensor readings, as well as selected historical reading buffered in the FRAM memory are broadcast periodically as BLE beacons.
The sensor node operates fully autonomous by harvesting from ambient light. It is implemented as *transient sensor node*: it only accumulates the minimal energy in a tiny SMD buffer capacitor required for a single sense and broadcast operation of a few milliseconds, before shutting down again. The on-board energy management unit (EMU) enables efficient operation and run-time reconfiguration of the applications energy and voltage requirements. A separate backup power domain for an nano-power real-time clock (RTC) allows persistent time keeping across multiple hours of energy unavailability.
Despite its compact design, the sensor node offers all necessary connections for debugging, power measurement, and system state monitoring with ease.


## Hardware Features

Transiently powered BLE sensor node platform with:
 - Energy Management Unit
 - RTC backup domain with recharge switch
 - Non-volatile FRAM
 - Wide range of sensor (temperature, humidity, pressure, light)
 - Bluetooth Low Energy radio


## Revisions

 - v1.0 (Oliver Brunecker): Version designed and manufactured during semester thesis at TIK
 - v1.1 (Oliver Brunecker): Partial integration of application side bugfixes in schematic, not manufactured
 - v2.0 (Lukas Sigrist/Alfonso Blanco): Redesigned version for batch manufacturing


## Project Contributors

* Alfonso Blanco <alfonso.blanco@ee.ethz.ch>
* Oliver Brunecker <brolive@student.ethz.ch>
* Andres Gomez <gomeza@ethz.ch>
* Lukas Sigrist <lukas.sigrist@tik.ee.ethz.ch>

Contact: Lukas Sigrist
